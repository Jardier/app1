import { Component, OnInit, EventEmitter, Output, OnDestroy, Input } from '@angular/core';
import { Frase } from '../shared/frase.model';
import { FRASES } from './frases-mock';
import { Coracao } from '../shared/coracao.model';
import { TentativasComponent } from '../tentativas/tentativas.component';



@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.css']
})
export class PainelComponent implements OnInit, OnDestroy {
 
  public frases : Frase[] = FRASES;
  public instrucao : string = 'Traduza a frase:';
  public resposta : string = '';

  public rodada : number = 0;
  public rodadaFrase : Frase;

  public progresso : number = 0;

  public tentativas : number;

  
  @Output()
  public encerrarJogo : EventEmitter<string> = new EventEmitter();

  constructor() {
    this.atualizarRodada();   
    
  }

  ngOnInit() {
    this.tentativas = new TentativasComponent().qtdCoracoes();
    
  }

  ngOnDestroy(): void {
    
  }
  public atualizaResposta(resposta : Event) : void {
    this.resposta = (<HTMLInputElement>resposta.target).value.trim();  
  }

  public verificarResposta() : void {
   
    if(this.rodadaFrase.frasePtBR === this.resposta) {
      //incremento a rodada
      this.rodada ++;
      //se rodada for igual a quantidade de frases
      if(this.rodada === this.frases.length) {
        this.encerrarJogo.emit('Vitória');
      }
      //atribui o novo valor a frase
      this.rodadaFrase = this.frases[this.rodada];
      //incremento o progresso basedo tamanho da frase
      this.progresso += (100/this.frases.length);
      
      this.atualizarRodada();
      
    } else {
      this.tentativas --;
      if(this.tentativas === -1){
        this.encerrarJogo.emit('Derrota');
      }
    }
   
  }

  public atualizarRodada() : void {
    this.rodadaFrase = this.frases[this.rodada];
    this.resposta = '';
  }
}
